'use strict';

const path = require('path');
const webpack = require('webpack');

const env = require('./env');

module.exports = {
  output: {
    path: path.join(process.cwd(), 'dist'),
    filename: '[name].js',
  },
  node: {
    __dirname: false,
    __filename: false,
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.json'],
  },
  devtool: 'source-map',
  plugins: [new webpack.DefinePlugin(env)],
};
