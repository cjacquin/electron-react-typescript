const merge = require('webpack-merge');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const baseConfig = require('./webpack.renderer.config');

module.exports = merge.smart(baseConfig, {
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        include: [path.join(process.cwd(), 'src')],
        exclude: [path.join(process.cwd(), 'src', 'main.ts')],
        loader: 'awesome-typescript-loader',
      },
    ],
  },
  plugins: [new UglifyJsPlugin()],
});
