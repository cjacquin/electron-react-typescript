const fs = require('fs');
const path =require('path');
const envPath = path.join(process.cwd(), 'env');

const dotenvFiles = [
  path.join(envPath, `${process.env.NODE_ENV}.env`),
  process.env.NODE_ENV !== 'test' && path.join(envPath, 'local.env'),
].filter(Boolean);

dotenvFiles.forEach(dotenvFile => {
  if (fs.existsSync(dotenvFile)) {
    require('dotenv-expand')(
      require('dotenv').config({
        path: dotenvFile,
      })
    );
  }
});

module.exports = Object.keys(process.env)
  .reduce((acc, key) => ({
    ...acc,
    [key]: JSON.stringify(process.env[key]),
  }), {})
