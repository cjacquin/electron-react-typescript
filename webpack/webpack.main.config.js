const path = require('path');
const merge = require('webpack-merge');

const baseConfig = require('./webpack.base.config');

module.exports = merge.smart(baseConfig, {
  target: 'electron-main',
  entry: {
    main: path.join(process.cwd(), 'src/main.ts'),
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        include: [path.join(process.cwd(), 'src', 'main.ts')],
        loader: 'awesome-typescript-loader',
      },
    ],
  },
});
