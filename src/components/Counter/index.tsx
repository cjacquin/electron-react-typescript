import React from 'react';

const redCubeImg = require('./RedCube.jpg');

export interface Props {
  value: number;

  incrementValue: () => any;
  decrementValue: () => any;
}

const Counter: React.SFC<Props> = ({
  value,
  incrementValue,
  decrementValue,
}) => (
  <div>
    <p>
      <img src={redCubeImg} />
    </p>
    <p>Current value: {value}</p>
    <p>
      <button onClick={incrementValue}>Increment</button>
      <button onClick={decrementValue}>Decrement</button>
    </p>
  </div>
);

export default Counter;
