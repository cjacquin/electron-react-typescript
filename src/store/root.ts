import { createFocusableStore } from 'lenrix';
import { applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';

import { initialState, RootState } from './state';

const middlewares: any[] = [];
const enhancer = composeWithDevTools(applyMiddleware(...middlewares));

export const configureStore = () => {
  switch (process.env.NODE_ENV) {
    case 'development':
    default:
      return createFocusableStore<RootState>(
        state => state || initialState,
        initialState,
        enhancer,
      );
  }
};

export const rootStore = configureStore();
