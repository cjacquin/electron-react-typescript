import { CounterState, counterState } from '../screens/Counter/state';

export interface RootState {
  counter: CounterState;
}

export type Update<State> = () => (state: State) => State;

export type Updates<Actions, State> = Partial<
  Record<keyof Actions, Update<State>>
>;

export type Effects<Actions> = Partial<
  Record<keyof Actions, (foo: Actions[keyof Actions]) => void>
>;

export const initialState: RootState = { counter: counterState };
