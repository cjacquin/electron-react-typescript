import { createHashHistory } from 'history';
import { Effects } from './state';

export interface NavigationActions {
  push: string;
  replace: string;
  back: any;
}

export const history = createHashHistory();

export const effects: Effects<NavigationActions> = {
  push: history.push,
  replace: history.replace,
  back: history.goBack,
};
