import React from 'react';
import ReactDOM from 'react-dom';
import { setObservableConfig } from 'recompose';
import { from, Observable } from 'rxjs';
import { Router } from 'react-router';

import { history } from './store/navigation';
import Layout from './layout';

setObservableConfig({
  fromESObservable: from,
  toESObservable: (stream: Observable<any>) => stream,
});

const mainElement = document.createElement('div');
document.body.appendChild(mainElement);

ReactDOM.render(
  <Router history={history}>
    <Layout />
  </Router>,
  mainElement,
);
