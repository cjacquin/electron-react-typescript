import React from 'react';
import { hot } from 'react-hot-loader';
import { Route } from 'react-router';
import { Link } from 'react-router-dom';

import Counter from '../screens/Counter';
import Home from '../screens/Home';

export const Layout: React.SFC<any> = () => (
  <React.Fragment>
    <header>
      <Link to="/home">Home</Link>
      <Link to="/Counter">Counter</Link>
    </header>
    <main>
      <Route path="/home" component={Home} />
      <Route path="/counter" component={Counter} />
    </main>
  </React.Fragment>
);

export default hot(module)(Layout);
