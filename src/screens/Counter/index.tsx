import React from 'react';
import { componentFromStream } from 'recompose';
import { map } from 'rxjs/operators';
import { hot } from 'react-hot-loader';

import Counter from '../../components/Counter';
import { counterStore } from './store';

const increment = (): void => counterStore.dispatch({ increment: undefined });
const decrement = (): void => counterStore.dispatch({ decrement: undefined });

export default hot(module)(
  componentFromStream(() =>
    counterStore.pick('value').pipe(
      map(({ value }) => (
        <React.Fragment>
          <Counter
            value={value}
            decrementValue={decrement}
            incrementValue={increment}
          />
        </React.Fragment>
      )),
    ),
  ),
);
