import { rootStore } from '../../store/root';
import { CounterActions, updates } from './updates';

export const counterStore = rootStore
  .focusPath('counter')
  .actionTypes<CounterActions>()
  .updates(updates);

if (module.hot) {
  module.hot.accept('./updates', () => {
    counterStore.hmrUpdate({
      handlers: require('./updates').updates,
    });
  });
}
