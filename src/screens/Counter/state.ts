export interface CounterState {
  readonly value: number;
}

export const counterState: CounterState = { value: 0 };
