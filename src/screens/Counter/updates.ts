import { Updates } from '../../store/state';
import { CounterState } from './state';

export interface CounterActions {
  increment: void;
  decrement: void;
}

export const updates: Updates<CounterActions, CounterState> = {
  decrement: () => state => ({ ...state, value: state.value - 1 }),
  increment: () => state => ({ ...state, value: state.value + 1 }),
};
